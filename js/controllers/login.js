/**
 ********** Created by Surbhi Goel ************
 **/

function loginCtrl($state,$timeout,$localStorage){

    login = this;
    login.account = {};
    login.spinners = false;

    if(!$localStorage.flyrobe){
        $localStorage.flyrobe = {};
        $localStorage.flyrobe.email = "";
    }

    login.loginToDashboard = loginToDashboard; //function to login

    /**********************************************
                function to login
     **********************************************/
    function loginToDashboard(){

        $state.go('tables.customerCare');

    }

}


angular
    .module('flyrobe')
    .controller('loginCtrl', loginCtrl);