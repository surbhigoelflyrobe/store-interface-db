/**
 * FLYROBE - Responsive Admin Theme
 *
 */
(function () {
    angular.module('flyrobe', [
        'ngStorage',
        'ui.router',                    // Routing
        'oc.lazyLoad',                  // ocLazyLoad
        'ui.bootstrap',                 // Ui Bootstrap
        'pascalprecht.translate',       // Angular Translate
        'ngIdle',                       // Idle timer
        'ngSanitize',                 // ngSanitize
        '720kb.datepicker'
    ])
})();

// Other libraries are loaded dynamically in the config.js file using the library ocLazyLoad