
/**
 * MainCtrl - controller
 * Contains several global data used in different view
 *
 */
function MainCtrl($rootScope,$http,$localStorage,$state,$scope,$filter) {
    mainVm=this;

    /**
     * slideInterval - Interval for bootstrap Carousel, in milliseconds:
     */
    this.slideInterval = 5000;

    mainVm.logout = logout; //function to logout

    mainVm.api              = "http://ops.flyrobeapp.com/api/web/v1/";
    // mainVm.api                 = "http://staging.flyrobeapp.com/api/web/v1/";


    mainVm.urlList          = {
                                    "url": ""
                              };
    mainVm.headers          = {
                                 'Authorization' : 'Bearer dashboard',
                                 'Content-Type'  : 'application/json'
                              };


    /**********************************************
     function to logout
     **********************************************/
    function logout(){
        delete $localStorage.flyrobe;
        $state.go('login');
    }

}


/**
 *
 * Pass all functions into module
 */
angular
    .module('flyrobe')
    .controller('MainCtrl', MainCtrl)
.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
            items.forEach(function(item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});







